import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigService } from './config.service';
import { DbService } from './db.service';


export interface user {
  name: string;
  age: number
}

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService, ConfigService, DbService],
})
export class AppModule {}
