import { Controller, Get, Inject } from '@nestjs/common';
import { IConfigType } from './types/configService';

@Controller()
export class AppController {
  constructor(@Inject('configService') private configService: IConfigType) {}
  @Get()
  get() {
    return `service ip: ${this.configService.url}, host: ${this.configService.host}`
  }
}