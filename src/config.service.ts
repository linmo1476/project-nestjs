
import { developmentConfig } from "./config/development.config";
import { productionConfig } from "./config/production.config";
import * as dotenv from "dotenv";
import * as path from "path";

dotenv.config({ path: path.join(__dirname, '../.env') })

export const ConfigService = {
    provide: 'configService',
    useValue: process.env.NODE_ENV === 'development' ? developmentConfig : productionConfig
}